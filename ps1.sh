#!/bin/bash


# Show full status every five minutes
FULL_STATUS_TIMEOUT=300
BRANCH_BG=25
REPO_BG=75
UPDOWN_BG=243

echo PS1_LAST_FULL_STATUS=0 > ~/.ps1_cache
echo PS1_FULL_STATUS_DIR=$(pwd) >> ~/.ps1_cache

PS1_LAST_FULL_STATUS=0
PS1_FULL_STATUS_DIR=$(pwd)

g(){
	# TODO: Be more robust about different repo name sources. This works for me.
	REPO=$(git remote -v 2>/dev/null | sed 's#origin#0#' | sort | grep '(push)' | awk '{ print $2 }' | rev | cut -d/ -f1-2 | cut -d. -f2- | rev )
	BRANCH=$(git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/')
    AB=$(git status --porcelain=v2 --branch 2>/dev/null | grep '# branch.ab' | cut -d' ' -f3- )

    UPDOWN=
    if [[ ${AB} != '+0 -0' ]]; then
        UPDOWN="\\033[48;5;$UPDOWN_BG;38;5;255m $AB "
    fi

	if [[ ! -z ${REPO} ]]; then
		echo -e "\\033[48;5;$REPO_BG;38;5;255m $REPO \\033[48;5;$BRANCH_BG;38;5;255m $BRANCH $UPDOWN\\033[0m"
	else
		echo Not a git repository
	fi
}

git_prompt() {
	source ~/.ps1_cache

	STATUS=$(git status -s 2>/dev/null)

	if [[ $? -eq 0 ]]; then
	
		AB=$(git status --porcelain=v2 --branch 2>/dev/null | grep '# branch.ab' | cut -d' ' -f3- )
		AHEAD=$(echo ${AB}|cut -d' ' -f1|cut -c2-)
		BEHIND=$(echo ${AB}|cut -d' ' -f2|cut -c2-)

		if [[ ${AHEAD} -gt 0 && ${BEHIND} -gt 0 ]]; then
			CHAR="↕"
		elif [[ ${AHEAD} -gt 0 ]]; then
			CHAR="↑"
		elif [[ ${BEHIND} -gt 0 ]]; then
			CHAR="↓"
		else
			CHAR="■"
		fi

		NOW=$(date +%s)
		if [[ $((NOW-PS1_LAST_FULL_STATUS)) -ge ${FULL_STATUS_TIMEOUT} || $(pwd) != ${PS1_FULL_STATUS_DIR} ]]; then
			PS1_LAST_FULL_STATUS=${NOW}
			g # not a typo. This calls g() above :D
		fi

		if [[ -z ${STATUS} ]]; then
			printf "$CHAR "
		else
			printf "\001\033[38;5;9m\002${CHAR}\001\033[0m\002 "
		fi

	fi

	echo PS1_LAST_FULL_STATUS=${PS1_LAST_FULL_STATUS} > ~/.ps1_cache
	echo PS1_FULL_STATUS_DIR=$(pwd) >> ~/.ps1_cache
}

export PS1="\$(git_prompt)\h:\W \u\$ "
